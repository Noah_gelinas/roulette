import java.util.Scanner;
public class Roulette {
    
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);

        RouletteWheel wheel=new RouletteWheel();
        int money=1000;

        int BetOrNot=0;
        int BetNum=0;   
        int betting=0;
        int playAgain=1;

        while (playAgain==1){
            BetOrNot=0;
            while(BetOrNot==0) {
            System.out.println("would you like to bet? Enter 1 for yes and 2 for no.");
            BetOrNot=sc.nextInt();
                if(BetOrNot==1) {
                    System.out.println("How much would you like to bet?");
                    System.out.println("You have " + money+"$ left.");
                    betting=sc.nextInt();
                    if(betting>money || betting<0){
                        System.out.println("invalid Ammount. Enter a number between 0 and "+ money);
                        betting=sc.nextInt();
                    }
                    System.out.println("You have " + (money-betting) + "$ left.");
                    System.out.println("Which number between 0 and 37 not including 37 would you like to bet on?");
                    BetNum=sc.nextInt();
                    while(BetNum>36 || BetNum<0) {
                        System.out.println("This is not a valid number on the wheel enter again.");
                        BetNum=sc.nextInt();
                    }
            
                }else if(BetOrNot==2) {
                    System.out.println("You have "+money+ "$ left.");
                    System.out.println("Which number between 0 and 37 not including 37 would you like to bet on?");
                    BetNum=sc.nextInt();

                }else{
                    System.out.println("Wrong input!");
                    BetOrNot=0;
                }
            }

            wheel.spin();
            int numFromWheel=wheel.getValue();

            if (numFromWheel==BetNum) {
                money=money+(betting*35);
                System.out.println(numFromWheel + " was spun on the wheel!");
                System.out.println("YOU WON!!!");
                System.out.println("You have "+money+"$ left.");
            }else{
                money=money-betting;
                System.out.println(numFromWheel + " was spun on the wheel!");
                System.out.println("You lost :(");
                System.out.println("You have "+money+"$ left.");
            }

            int wrongValues=1;
            while(wrongValues==1){
                System.out.println("Would you like to play again? Enter 1 for yes and 2 for no.");
                playAgain=sc.nextInt();

                    if (playAgain==1){
                        System.out.println("See you back at the start of the game!");
                        wrongValues=0;
                    }else if(playAgain==2) {
                        System.out.println("Thanks for playing!");
                        wrongValues=0;
                    }else{
                        System.out.println("Bad input enter 1 or 2 please.");                                               
                    }   
            }

        
        }
        System.out.println("You are leaving with "+money+"$ left.");

        if (money<1000){
            System.out.println("You lost "+ (1000-money)+"$");
        }else{
            System.out.println("You won "+(money-1000)+"$");
        }
    }
}