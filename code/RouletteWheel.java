import java.util.Random;
public class RouletteWheel {
    private int number=0;
    private Random randomNum;

    public RouletteWheel() {
        this.randomNum=new Random();
    }


    public void spin() {
        this.number=randomNum.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }
}
